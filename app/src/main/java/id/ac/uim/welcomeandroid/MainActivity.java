package id.ac.uim.welcomeandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.username)
    EditText etUsername;

    @BindView(R.id.password)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username = etUsername.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();

                loginUser(username, password);
            }
        });

    }

    private void loginUser(String username, String password) {

        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)){

            if (username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admo")){

                Toast.makeText(MainActivity.this, "Login Sukses", Toast.LENGTH_LONG).show();

                //Membuka activity lain
                Intent in = new Intent(MainActivity.this, UserActivity.class);
                startActivity(in);

            }else {

                Toast.makeText(MainActivity.this, "Username atau password salah", Toast.LENGTH_LONG).show();

            }

        }else {

            Toast.makeText(MainActivity.this, "Username atau Password Tidak Boleh Kosong", Toast.LENGTH_LONG).show();

        }

    }
}
